#!/usr/bin/python3

import os
import re
import sys
import glob
import mutagen
import requests
import wikipedia
from typing import Tuple, List
from youtube_dl import YoutubeDL

# constants

error_tries = 2
duration_tries = 100 # number of tries to find a video with a suitable duration
duration_tolerance = 30 # tolerance in seconds to consider a video duration suitable

# types

Song = Tuple[str, int] # name, duration
Album = Tuple[str, str, List[Song]] # name, cover, songs
Artist = Tuple[str, str, List[Album]] # name, cover, albums

# utils

def print_artist(artist:Artist) -> None:
    artist_name, artist_cover, albums = artist
    print(artist_name)
    for album in albums:
        album_name, album_cover, songs = album
        print('\t' + album_name)
        for song in songs:
            song_name, duration = song
            print('\t\t' + song_name)

def get_soup(url:str) -> wikipedia.BeautifulSoup:
    return wikipedia.BeautifulSoup(requests.get(url).text, 'html.parser')

# / (solidus U+002F) are illegal in unix filenames, so replace them with ⁄ (fraction slash U+2044)
def safe_filename(string:str) -> str:
    return string.replace('/', '⁄')

# wikipedia parsing

def parse_cover(infobox) -> str:
    # get the hightest resolution
    image = infobox.find(class_='image')
    if image:
        soup = get_soup('https://en.wikipedia.org' + image['href'])
        return 'https:' + soup.find(class_='internal')['href']

def parse_album(url:str) -> Album:
    soup = get_soup(url)
    infobox = soup.find(class_='infobox') # infobox is the card on the right
    album_name = infobox.find(class_='summary').string.strip('"') # the name of the album is the title of the infobox
    album_cover = parse_cover(infobox)
    descriptions = list(infobox.find_all('th', class_='description'))
    if descriptions[0].a.string.startswith('Single'): # is a single
        if descriptions[1].find(string=re.compile('from the')): # part of an album, skip
            return
    songs = []
    tracklist = soup.find(class_='tracklist') # get the first one (could be improved ?)
    if tracklist:
        for track_th in tracklist.find_all(id=re.compile('track')):
            cells = list(track_th.next_siblings)
            song_name_parts = cells[0].strings
            song_name_parts = filter(lambda s : not (s[0] == '[' and s[-1] == ']'), song_name_parts) # remove all [42] ...
            song_name = ''.join(song_name_parts)
            song_name = song_name.replace('"', '') # remove all "
            duration_strings = cells[-1].string.partition(':')
            duration = int(duration_strings[0]) * 60 + int(duration_strings[2])
            songs.append((song_name, duration))
    else: # no tracklist get single song from infobox
        duration_span = infobox.find(class_='duration')
        minutes = int(duration_span.find(class_='min').string)
        seconds = int(duration_span.find(class_='s').string)
        duration = minutes * 60 + seconds
        songs.append((album_name, duration))
    return (album_name, album_cover, songs)

def parse_artist(url:str) -> Artist:
    soup = get_soup(url)
    navbox = soup.find(class_='navbox') # navbox is the template at the bottom of the page (we take the first one)
    artist_name = navbox.find(class_='fn org').string # the name of the artist is the title of the navbox
    infobox = soup.find(class_='infobox') # infobox is the card on the right
    artist_cover = parse_cover(infobox)
    albums = []
    for section in navbox.find_all(class_='navbox-group'):
        if section.string in ('Studio albums', 'Compilation album', 'Special albums', 'Extended plays', 'EPs', 'Singles', 'Single albums'):
            for a in section.next_sibling.find_all('a'):
                album = parse_album('https://en.wikipedia.org' + a['href'])
                if album: # album is none if it's a single which is part of an album
                    albums.append(album)
    return (artist_name, artist_cover, albums)

# youtube ripping

def download_cover(cover:str, filename:str) -> None:
    extension = cover.rpartition('.')[2]
    with open(filename + '.' + extension, 'wb') as file:
        file.write(requests.get(cover).content)

def download_song(artist_name:str, album_name:str, index:int, song:Song) -> None:
    song_name, duration = song
    song_filename = str(index + 1) + '.' + safe_filename(song_name) # add index in front of filename
    found = False
    for duration_try in range(duration_tries):
        for error_try in range(error_tries):
            print('searching youtube for "' + artist_name + ' ' + song_name + '" attempt ' + str(duration_try + 1) + ' : ', end='')
            options = {
                'quiet':True,
                'format':'bestaudio',
                'postprocessors':[{'key':'FFmpegExtractAudio'}],
                'outtmpl':song_filename.replace('%', '%%') + '.%(ext)s',
                'playlist_items':str(duration_try + 1),
            }
            query = 'ytsearch' + str(duration_tries) + ':' + artist_name + ' ' + song_name
            info = YoutubeDL(options).extract_info(query, download=False)
            if len(info['entries']) == 0: # why is this happening ?
                print('error')
                continue
            else:
                info = info['entries'][0]
                video_duration = int(info['duration'])
                duration_delta = abs(video_duration - duration)
                if duration_delta <= duration_tolerance:
                    print('success (' + str(video_duration) + '~=' + str(duration) + ')')
                    found = True
                    # download
                    YoutubeDL(options).download([info['webpage_url']])
                    # ytdl doesn't report the correct extension so this is the only way
                    full_song_filename = glob.glob(glob.escape(song_filename) + '.*')[0]
                    # add metadata
                    mutafile = mutagen.File(full_song_filename)
                    mutafile['title'] = song_name
                    mutafile['album'] = album_name
                    mutafile['artist'] = artist_name
                    mutafile['tracknumber'] = str(index + 1)
                    mutafile.pop('language', None) # language is often wrong, so just erase it
                    mutafile.save()
                else:
                    print('failed (' + str(video_duration) + '!=' + str(duration) + ')')
                break
        if found:
            break
    if not found:
        # create an empty file to know their is a missing file
        with open(song_filename + '.' + 'missing', 'w'):
            pass

def download_album(artist_name:str, album:Album) -> None:
    album_name, album_cover, songs = album
    album_filename = safe_filename(album_name)
    os.mkdir(album_filename)
    os.chdir(album_filename)
    if album_cover:
        print('downloading cover for "' + artist_name + ' ' + album_name + '" from wikipedia')
        download_cover(album_cover, album_filename)
    for index, song in enumerate(songs):
        download_song(artist_name, album_name, index, song)
    os.chdir('..')

def download_artist(artist:Artist) -> None:
    artist_name, artist_cover, albums = artist
    artist_filename = safe_filename(artist_name)
    os.mkdir(artist_filename)
    os.chdir(artist_filename)
    if artist_cover:
        print('downloading cover for "' + artist_name + '" from wikipedia')
        download_cover(artist_cover, artist_filename)
    for album in albums:
        download_album(artist_name, album)
    os.chdir('..')

# main

if len(sys.argv) == 1:
    print('usage : ' + sys.argv[0] + ' [ARTISTS]')
else:
    for arg in sys.argv[1:]:
        print('parsing wikipedia to find "' + arg + '"\'s discography')
        try:
            artist = parse_artist(wikipedia.page(arg + ' group').url) # search wikipedia for the best page
        except:
            print('error : unable to find "' + arg + '"\'s discography')
            artist = None
        if artist:
            try:
                download_artist(artist)
            except FileExistsError as e:
                print('error : file already exists "' + e.filename + '"')
